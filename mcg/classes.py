#! /usr/bin/env python3
# coding: utf-8

import sys

import random

import pygame
from pygame.locals import *

from mcg.locals import *

class Level:
    """
        Create the maze's map and display it
    """
    def __init__(self, file):
        self.file = file
        self.structure = 0

        # Coordinates of start tile
        self.start_x = 0
        self.start_y = 0

        # Count of ground tiles for random range generation
        self.ground_count = 0
        self.ground_rand = []
        self.items = []

        # Items init
        self.item_n = 1
        self.item_p = 1
        self.item_e = 1

    def item_n_found(self):
        self.item_n = 0

    def item_p_found(self):
        self.item_p = 0

    def item_e_found(self):
        self.item_e = 0

    def check_success(self):
        return (self.item_e + self.item_p + self.item_n) == 0

    def generate(self):
        """
            Generate level with file entry
        """
        self.ground_count = 0
        ind = 0
        self.items = ["N", "P", "E"]
        with open(self.file, "r", encoding="UTF-8") as file:
            level_structure = []
            for line in file:
                level_line = []
                for tile in line:
                    if tile != "\n":
                        if tile == "G":
                            if ind <= len(self.ground_rand) - 1 \
                            and self.ground_count == self.ground_rand[ind]:
                                tile = self.items[ind]
                                ind += 1
                            self.ground_count += 1
                        level_line.append(tile)
                level_structure.append(level_line)
            self.structure = level_structure

    def rand_item(self, nmb_items):
        """
            Count number of G tiles and generate random value
        """
        with open(self.file, "r", encoding="UTF-8") as file:
            for line in file:
                for tile in line:
                    if tile != "\n":
                        if tile == "G":
                            self.ground_count += 1
        self.ground_rand = sorted(random.sample(range(1, self.ground_count), nmb_items))

    def load_resources(self):
        """
            Load the level's images before use them.
        """
        self.ground = pygame.image.load(GROUND_IMG).convert()
        self.wall = pygame.image.load(WALL_IMG).convert()
        self.start = pygame.image.load(START_IMG).convert()
        self.finish = pygame.image.load(FINISH_IMG).convert()
        self.needle = pygame.image.load(NEEDLE_IMG).convert_alpha()
        self.pipe = pygame.image.load(PIPE_IMG).convert_alpha()
        self.ether = pygame.image.load(ETHER_IMG).convert_alpha()
        self.win = pygame.image.load(WIN).convert_alpha()
        self.lose = pygame.image.load(LOSE).convert_alpha()

    def display(self, window):
        """
            Display the structured maze.
            Expect as argument the current window.
        """
        num_line = 0
        ind = 0
        for line in self.structure:
            num_tile = 0
            for tile in line:
                x = num_tile * TILE_SIZE
                y = num_line * TILE_SIZE
                if tile == "W":
                    window.blit(self.wall, (x, y))
                elif tile == "G":
                    self.ground_count += 1
                    window.blit(self.ground, (x, y))
                elif tile == "S":
                    # Set coordinate of start tile
                    self.start_x = x
                    self.start_y = y
                    self.start_x_tile = num_tile
                    self.start_y_tile = num_line
                    window.blit(self.start, (x, y))
                elif tile == "F":
                    window.blit(self.finish, (x, y))
                elif tile == "N":
                    window.blit(self.ground, (x, y))
                    if self.item_n == 1 :
                        window.blit(self.needle, (x, y))
                elif tile == "P" :
                    window.blit(self.ground, (x, y))
                    if self.item_p == 1 :
                        window.blit(self.pipe, (x, y))
                elif tile == "E" :
                    window.blit(self.ground, (x, y))
                    if self.item_e == 1 :
                        window.blit(self.ether, (x, y))
                num_tile += 1
            num_line += 1

class Character:
    """
        Display character and manage his movements.
    """

    def __init__(self, image, level, startx, starty, start_x_tile, start_y_tile):

        # Sprite of character's player
        self.character = pygame.image.load(image).convert_alpha()

        # Start position
        self.tile_x = start_x_tile
        self.tile_y = start_y_tile
        self.x_coord = startx
        self.y_coord = starty

        # Structure level
        self.level = level

    def move_to(self, direction):
        """
        Move character, update sprite coordinate
        """
        if direction == "right":
            if self.tile_x < (TILE_NUMBER - 1):
                if self.level.structure[self.tile_y][self.tile_x+1] != 'W':
                    self.tile_x += 1
                    self.x_coord = self.tile_x * TILE_SIZE
        if direction == "left":
            if self.tile_x > 0:
                if self.level.structure[self.tile_y][self.tile_x-1] != 'W':
                    self.tile_x -= 1
                    self.x_coord = self.tile_x * TILE_SIZE
        if direction == "up":
            if self.tile_y > 0:
                if self.level.structure[self.tile_y-1][self.tile_x] != 'W':
                    self.tile_y -= 1
                    self.y_coord = self.tile_y * TILE_SIZE
        if direction == "down":
            if self.tile_y < (TILE_NUMBER - 1):
                if self.level.structure[self.tile_y+1][self.tile_x] != 'W':
                    self.tile_y += 1
                    self.y_coord = self.tile_y * TILE_SIZE
