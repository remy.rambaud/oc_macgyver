# Constants for MacGyver

TILE_SIZE = 20
TILE_NUMBER = 15
WINDOW_SIZE = TILE_NUMBER * TILE_SIZE
ITEM_NUMBER = 3
LEVEL_MAP = "ressources/levels/level_map"

# Images
CHARACTER_IMG = "ressources/images/character.png"
WALL_IMG = "ressources/images/wall.png"
GROUND_IMG = "ressources/images/ground.png"
START_IMG = "ressources/images/start.png"
FINISH_IMG = "ressources/images/finish.png"
NEEDLE_IMG = "ressources/images/needle.png"
PIPE_IMG = "ressources/images/pipe.png"
ETHER_IMG = "ressources/images/ether.png"
WIN = "ressources/images/win.png"
LOSE = "ressources/images/lose.png"
