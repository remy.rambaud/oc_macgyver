#! /usr/bin/env python3
# coding: utf-8

"""
Projet MacGyver

Jeu vidéo créé dans le cadre du projet 3 de la formation DA Python
"""

import sys

import pygame
from pygame.locals import *

import mcg.classes as mcg
from mcg.locals import *

pygame.init()

def main():
    """
        Main code
    """

    # Framerate limit
    clock = pygame.time.Clock()

    # Key repeat
    pygame.key.set_repeat(50, 110)

    # Create main window
    window = pygame.display.set_mode((WINDOW_SIZE, WINDOW_SIZE))
    pygame.display.set_caption("Maze Runner: The Death Cure")

    # Display background en generate maze
    level = mcg.Level(LEVEL_MAP)
    level.rand_item(ITEM_NUMBER)
    level.generate()
    level.load_resources()
    level.display(window)

    # Find start tile coordinate, generate character's player and place it on the start tile
    start_x = level.start_x
    start_y = level.start_y
    start_x_tile = level.start_x_tile
    start_y_tile = level.start_y_tile
    player = mcg.Character(CHARACTER_IMG, level, start_x, start_y, start_x_tile, start_y_tile)
    window.blit(player.character, (player.x_coord, player.y_coord))
    pygame.display.flip()

    # Main loop, continue until quit event encountered
    while True:
        # Limit framate to 30
        clock.tick(30)

        # Call the method "move_to" according to keyboard events
        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                sys.exit()
            elif event.type == KEYDOWN:
                if event.key == K_RIGHT:
                    player.move_to("right")
                elif event.key == K_LEFT:
                    player.move_to("left")
                elif event.key == K_DOWN:
                    player.move_to("down")
                elif event.key == K_UP:
                    player.move_to("up")

        # Update display with player's new coordinates
        level.display(window)
        window.blit(player.character, (player.x_coord, player.y_coord))
        pygame.display.flip()

        # Victory condition, delay applyed before exiting
        if level.structure[player.tile_y][player.tile_x] == "N":
            level.item_n_found()
        elif level.structure[player.tile_y][player.tile_x] == "P":
            level.item_p_found()
        elif level.structure[player.tile_y][player.tile_x] == "E":
            level.item_e_found()
        elif level.structure[player.tile_y][player.tile_x] == "F":
            pygame.display.flip()
            if level.check_success():
                window.blit(level.win, (0, 0))
                pygame.display.flip()
                pygame.time.delay(2000)
                sys.exit()
            else:
                window.blit(level.lose, (0, 0))
                pygame.display.flip()
                pygame.time.delay(2000)
                sys.exit()

if __name__ == '__main__':
    main()
